'use strict';

describe('Directive: mdeSocialList', function() {

    // load the directive's module
    beforeEach(module('am-wb-seen-monitors'));

    // var element;
    var scope;

    beforeEach(inject(function($rootScope) {
	scope = $rootScope.$new();
    }));

    it('should attach a list of awesomeThings to the scope', function() {
	expect(angular.isDefined(scope)).toBe(true);
    });
});
