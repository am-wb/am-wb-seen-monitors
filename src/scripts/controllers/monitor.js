/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

class AmWbSeenMonitorGeneralCtrl {

	constructor($scope, $monitor, $interval, QueryParameter){
		// services
		this.$scope = $scope;
		this.$monitor = $monitor;
		this.$interval = $interval;

		this.queryParameter = new QueryParameter();

		this.initChartOptions();
		var ctrl = this;
		$scope.$watch('wbModel.metrics', function(metrics){
			ctrl.loadMetrics(metrics);
		}, true);

		$scope.$watch('wbModel.interval', function(value){
			if(value) {
				// to millisecond
				value = value * 1000; 
			}
			ctrl.setInterval(value);
		});

		$scope.$on('$destroy', function() {
			ctrl.destroy();
		});
	}

	/**
	 * Sets new interval
	 */
	setInterval(interval){
		var ctrl = this;
		if(this.intervalTask){
			this.$interval.cancel(this.intervalTask);
		}
		this.intervalTask = this.$interval(function() {
			ctrl.fetchNewData();
		}, interval || 1000 );
	}

	initChartOptions(){
		var ctrl = this;
		// Chart options
		var chart = {
				type : 'multiChart',
				height : 450,
				margin : {
					top : 20,
					right : 20,
					bottom : 50,
					left : 75
				},
				x : function (d) {
					return d[0]/1000;
				},
				y : function (d) {
					return d[1];
				},
				useVoronoi : false,
				clipEdge : false,
				duration : 250,
				useInteractiveGuideline : true,
				xAxis : {
					showMaxMin : false,
					axisLabel : 'Time',
					tickFormat : function(data){
						return ctrl.formatData(data, {
							type: 'DateTime',
							format: 'hh:mm:ss'
						});
					}
				},
				yAxis1 : {
					showMaxMin : false,
					tickFormat : function(data){
						return ctrl.formatData(data, {
							format: ',d'
						});
					}
				},
				yAxis2 : {
					showMaxMin : false,
					tickFormat : function(data){
						return ctrl.formatData(data, {
							format: ',d'
						});
					}
				},
				tooltip : {
					enabled : false
				}
		};

		// chart config
		var config = {
				visible: true,
				extended: false,
				disabled: false,
				refreshDataOnly: true,
				deepWatchOptions: true,
				deepWatchData: true,
				deepWatchDataDepth: 2,
				debounce: 10
		};

		// directive option
		var options = {
				chart: chart
		};

		// directive api
		var api = {};

		// bind to class
		this.options = options;
		this.chart = chart;
		this.api = api;
		this.config = config;

		this.$scope.api = api;
		this.$scope.config = config;
		this.$scope.options = options;

		return options;
	}

	setChartType(type){
		var chartType = null;
		switch(type){
			case 'pie': 
				chartType = 'pieChart';
				break;
			default:
				chartType = 'lineChart';
		}
		this.chart.type = chartType;
	}

	/**
	 * Loads metrics in the controller
	 */
	loadMetrics(metrics){
		this.metrics = metrics;
		this.queryParameter.clearFilters();
		// set data
		this.data = [];
		this.series = [];
		this.$scope.data = this.data;


		for(var i = 0; i < metrics.length; i++){
			var metric =  metrics[i];
			this.queryParameter.addFilter('name', metric.name);

			var series ={};
			series.key = metric.name;
			series.values = [];
			series.color = metric.color;
			series.type='line';
			series.yAxis = metric.rightSide ? 2 : 1;

			this.data.push(series);
			this.series[series.key] = series;
		}

	}

	/**
	 * Get new value of metrics
	 */
	fetchNewData(){
		var ctrl = this;
		this.$monitor.getMetrics(this.queryParameter)
		.then(function(result){
			ctrl.pushValues(result.items);
		});
	}

	/**
	 * Push new values
	 */
	pushValues(metricsValues){
		var d = new Date();
		var n = d.getTime();
		for(var i = 0; i < metricsValues.length; i++){
			var metric = metricsValues[i];
			var values = this.series[metric.name].values;
			values.push([n, metric.value || 0]);
			
			this.series[metric.name].values = values.slice(Math.max(values.length - 100, 0));
		}
	}

	destroy() {
		this.$interval.cancel(this.intervalTask);
	}
	
    formatData(data, option){
        if (option.type === 'DateTime'){
            var date = moment.unix(data);
            return date.format(option.format);
        }
        if (option.type === 'Time'){
            var sec_num = data; // don't forget the second param
            var hours   = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (minutes < 10) {
                minutes = '0'+minutes;
            }
            if (seconds < 10) {
                seconds = '0'+seconds;
            }
            if(hours === 0) {
                return minutes+':'+seconds;
            }
            if (hours   < 10) {hours   = '0'+hours;}
        }
        return d3.format(option.format || '.xx')(data);
    }
}

//TODO: maso, 2018: move to new verstion of angular
AmWbSeenMonitorGeneralCtrl.$inject = ['$scope', '$monitor', '$interval', 'QueryParameter'];



/*
 * Add to angularjs
 */
angular.module('am-wb-seen-monitors')
.controller('AmWbSeenMonitorGeneralCtrl', AmWbSeenMonitorGeneralCtrl);

