/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-seen-monitors').run(function($widget, $settings) {
	
	
	$settings.newPage({
		label: 'Monitor metrics',
		type: 'seen-monitor-metrics',
		templateUrl: 'views/am-wb-seen-monitor-settings/metrics.html',
		controller: 'AmWbSeenMonitorMetricsCtrl',
		controllerAs: 'ctrl',
		tags: ['data', 'monitor']
	});

	/**
	 * @ngdoc Widgets
	 * @name SeenStoragePie
	 * @description Storage pie chart view
	 */
	$widget.newWidget({
		type : 'SeenMonitorMetrics',
		title : 'Monitor metrics',
		description : 'Display list of monitor metrics as line chart and update last value',
		icon : 'pie_chart',
        groups: ['dashboard', 'monitor'],

		controller : 'AmWbSeenMonitorGeneralCtrl', 
		controllerAs: 'ctrl',

		templateUrl : 'views/am-wb-seen-monitor-widgets/chart.html',
		link : 'https://gitlab.com/weburger/am-wb-seen-calendar',
		setting : ['seen-monitor-metrics'],
		data : {}
	});



});
