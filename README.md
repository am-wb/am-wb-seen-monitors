# Monitors

master: 
[![pipeline status](https://gitlab.com/am-wb/am-wb-seen-monitors/badges/master/pipeline.svg)](https://gitlab.com/am-wb/am-wb-seen-monitors/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/60aafa1d89804c199ebafaea38d64b08)](https://www.codacy.com/app/am-wb/am-wb-seen-monitors?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=am-wb/am-wb-seen-monitors&amp;utm_campaign=Badge_Grade)


See more details in [technical document](https://am-wb.gitlab.io/am-wb-monitors/doc/index.html).